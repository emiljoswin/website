var express = require("express");
var app = express();

app.use(express.logger());
app.use(express.static('./views'));

app.set('views', __dirname + '/views');
app.set('view engine', 'jade');

app.get('/', function (req, res) {
  res.render('home');
});

app.get('/about', function (req, res) {
  res.render('about');
});


var port = 5000;
app.listen(port, function() {
   console.log("Listening on " + port);
});

